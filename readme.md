trpgen
===================

A stateless password generator written in Racket/gui.
Generate a password from a master key and a text salt.

![Screenshot](https://abload.de/img/suhs14.png)

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png) 

[Note: I am no longer working on this.]